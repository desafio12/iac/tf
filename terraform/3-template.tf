data "template_file" "k8s" {
  template = file("./templates/k8s.tpl")
  vars = {
    k3s_master_ip = "${join("\n", [for instance in digitalocean_droplet.masters : join("", [instance.ipv4_address, " internal_ip=", instance.ipv4_address_private, " ansible_ssh_private_key_file=", var.pvt_key])])}"
    k3s_node_ip   = "${join("\n", [for instance in digitalocean_droplet.workers : join("", [instance.ipv4_address, " internal_ip=", instance.ipv4_address_private, " ansible_ssh_private_key_file=", var.pvt_key])])}"
    k3s_node_db_ip   = "${join("\n", [for instance in digitalocean_droplet.workers_db : join("", [instance.ipv4_address, " internal_ip=", instance.ipv4_address_private, " ansible_ssh_private_key_file=", var.pvt_key])])}"
  }
}

resource "local_file" "k8s_file" {
  content  = data.template_file.k8s.rendered
  filename = "../inventory/cluster/hosts.ini"
}
