terraform {
  required_version = "~> 1.0"

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}


resource "digitalocean_ssh_key" "k8s_keys" {
  name       = "Chaves para o cluster k8s"
  public_key = var.ssh_k8s_keys
}

resource "digitalocean_droplet" "masters" {
  count       = var.num_k3s_masters
  name        = "k3s-master-${count.index}"
  region      = var.do_region
  image       = var.image
  size        = var.master_vm_size
  vpc_uuid    = digitalocean_vpc.vmbr_k8s.id
  monitoring  = true
  ipv6        = true
  tags        = ["k8s", "master-desafio"]
  graceful_shutdown = true
  ssh_keys    = [digitalocean_ssh_key.k8s_keys.fingerprint]
}

resource "digitalocean_droplet" "workers" {
  count       = var.num_k3s_nodes
  name        = "k3s-worker-${count.index}"
  region      = var.do_region
  image       = var.image
  size        = var.node_vm_size
  vpc_uuid    = digitalocean_vpc.vmbr_k8s.id
  monitoring  = true
  ipv6        = true
  tags        = ["k8s", "workers", "nodes-workers-app-desafio"]
  graceful_shutdown = true
  ssh_keys    = [digitalocean_ssh_key.k8s_keys.fingerprint]
}

output "Master-IPS" {
  description = "Imprime os ips dos droplets master gerados"
  value = [
        "${digitalocean_droplet.masters.*.ipv4_address}"
        ]
}
output "Worker-IPS" {
  description = "Imprime os ips dos droplets worker gerados"
  value = [
    "${digitalocean_droplet.workers.*.ipv4_address}"
  ]
}

