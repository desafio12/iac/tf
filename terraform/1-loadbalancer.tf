resource "digitalocean_loadbalancer" "lb_app" {
  name   = "loadbalancer-1"
  region = var.do_region
  vpc_uuid = digitalocean_vpc.vmbr_k8s.id

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_tag = "nodes-workers-app-desafio"
}
