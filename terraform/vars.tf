variable "do_token" {
 type = string 
 description = "Chave da API da Digital Ocean" 
 sensitive = true
 validation {
    condition     = length(var.do_token) > 60 
    error_message = "Token DO com tamanho invalido."
 }
}

variable "do_region" {
  type = string
  default = "nyc3" 
  description = "Região onde serão aplicados os recursos"
}

variable "ssh_k8s_keys" {
  type = string 
  description = "(Digital Ocean) Chave pública que sera importada para a DO, utilizada para acesso aos servidores"
  sensitive = true
}

variable "pvt_key" {
  type = string 
  default = "~/.ssh/desafio"
  description = "(Ansible) Caminho para a chave privada, que será utilizada para términar a configuracao dos servidores."
  sensitive = true
}


variable "num_k3s_masters" {
 type = number
 description = "Número de Nós Master"
 validation {
    condition     = (var.num_k3s_masters < 4 ) && (var.num_k3s_masters > 0 )
    error_message = "O número de master tem de ser superior a 0 e menor ou igual a 3."
 }
}

variable "master_vm_size" {
 type        = string 
 default     = "s-2vcpu-4gb-intel"
 description = "Tipo ou Tamanho (CPU,Memória,Disco), da VM/Droplet de acordo com o padrão especificado pela DO"
}


variable "num_k3s_nodes" {
 type = number
 description = "Número de Nós Worker. Onde rodarão as aplicações"
 validation {
    condition     = (var.num_k3s_nodes < 5 ) && (var.num_k3s_nodes > 0 )
    error_message = "O número máximo de nodes é 4."
 }
}

variable "node_vm_size" {
 type        = string 
 default     = "s-2vcpu-4gb-intel"
 description = "Tipo ou Tamanho (CPU,Memória,Disco), da VM/Droplet de acordo com o padrão especificado pela DO"
}

variable "num_k3s_nodes_db" { 
 type = number
 description = "Número de Nós Worker voltados para o banco. Onde rodarão os bancos de dados e terão discos adicionados"
 validation {
    condition     = (var.num_k3s_nodes_db < 4 ) && (var.num_k3s_nodes_db > 0 )
    error_message = "O número máximo de nodes é 3."
 }
}

variable "node_db_vm_size" {
 type        = string 
 default     = "s-2vcpu-4gb-intel"
 description = "Tipo ou Tamanho (CPU,Memória,Disco), da VM/Droplet de acordo com o padrão especificado pela DO"
}

variable "db_disk_size" {
 type = number
 default = 20
 description = "Tamanho em Gigas do disco que será vinculado ao nó worker do banco de dados"
 validation {
    condition     = (var.db_disk_size < 200 ) && (var.db_disk_size > 15 )
    error_message = "O tamanho máximo estabelecido é de 200G."
 }
}

variable "image" {
 type = string 
 default = "ubuntu-20-04-x64"
 description = <<-EOT
                Nome da imagem utilizada na criação do Droplet. 
                De posse do token pode usar o comando: curl -X GET --silent \"https://api.digitalocean.com/v2/images?per_page=999\" -H \"Authorization: Bearer $DO_API_TOKEN\"
                para listar as imagens disponíveis
            EOT
}
