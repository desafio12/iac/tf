resource "digitalocean_firewall" "k8s" {
    name = "k8s"
    tags = ["k8s"]

    inbound_rule {
       source_load_balancer_uids = [digitalocean_loadbalancer.lb_app.id]
       protocol = "tcp" 
       port_range = "22"
    }

    inbound_rule {
       source_load_balancer_uids = [digitalocean_loadbalancer.lb_app.id]
       protocol = "tcp" 
       port_range = "80"
    }
    inbound_rule {
       source_load_balancer_uids = [digitalocean_loadbalancer.lb_app.id]
       protocol = "tcp" 
       port_range = "443"
    }
    inbound_rule {
       source_tags = ["k8s"]
       protocol = "tcp"
       port_range            = "1-65535"
    }
    
    inbound_rule {
        source_tags = ["k8s"]
        protocol = "udp"
        port_range = "1-65535"
    }

    outbound_rule {
        protocol              = "icmp"
        destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
        protocol              = "udp"
        port_range            = "1-65535"
        destination_addresses = ["0.0.0.0/0", "::/0"]
    }

    outbound_rule {
        protocol              = "tcp"
        port_range            = "1-65535"
        destination_addresses = ["0.0.0.0/0", "::/0"]
    }
}

resource "digitalocean_firewall" "input_temporary" {
    name = "input-temporary"
    tags = ["k8s"]

    inbound_rule {
       source_addresses = ["0.0.0.0/0", "::/0"]
       protocol = "tcp" 
       port_range = "22"
    }
}
