resource "digitalocean_volume" "workers_db_storage" {
  region                   = var.do_region 
  count                    = var.num_k3s_nodes_db
  name                     = "k3s-worker-db-storage-${count.index}"
  size                     = var.db_disk_size 
  tags                     = ["k8s-db-storage","storage","db"]
  initial_filesystem_type  = "xfs"
  initial_filesystem_label = "database"
  description              = "volume para uso do banco de dados"
}


resource "digitalocean_droplet" "workers_db" {
  count       = var.num_k3s_nodes_db
  name        = "k3s-worker-db-${count.index}"
  region      = var.do_region
  image       = var.image
  size        = var.node_db_vm_size
  vpc_uuid    = digitalocean_vpc.vmbr_k8s.id
  monitoring  = true
  ipv6        = true
  tags        = ["k8s", "workers", "nodes-workers-db-desafio"]
  graceful_shutdown = true
  ssh_keys    = [digitalocean_ssh_key.k8s_keys.fingerprint]
}

resource "digitalocean_volume_attachment" "workers_storage_attachment" {
  count = var.num_k3s_nodes_db
  droplet_id = digitalocean_droplet.workers_db[count.index].id
  volume_id = digitalocean_volume.workers_db_storage[count.index].id
}

output "worker_db_IPS" {
  description = "Imprime os ips dos droplets worker db gerados"
  value = [
    "${digitalocean_droplet.workers_db.*.ipv4_address}"
  ]
}
