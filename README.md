# Criar infra completa usando Terraform e Ansible: gerando o cluster Kubernetes na Digital Ocean

Esse é um projeto derivado do repositório feito para o Proxmox: https://github.com/NatiSayada/k3s-proxmox-terraform-ansible

A ideia dele é implementar toda a estrutura necessaria:

- Cluster Kubernetes
- Regras de Firewall
- LoadBalancer para a entrada dos projetos
- Runner CI do GitLAB para receber as solicitações e publicar novos projetos, dentro dessa stack demilitarizada

* Atenção, em caso de redimensionamento da estrutura, na diminuição do numero de nós, antes é necessário definir no k8s o cordon e drain do nodo, bem como remover o nodo do cluster k8s. Aí sim proceder com o pipeline para remover os nós.

## Recursos Necessários

- Token de API da Digital Ocean
- GitLab para executar todo o processo, inclusive guarda o state do Terrafom

 * Todas as partes do processo utilizam recursos de container, evitando que seja necessário instalar quaisquer pre-requisito como Terraform ou Ansible no Runner.

### Digital Ocean

- Gere um Token Pessoal em: API -> Tokens/Keys
- Espaço Minimo necessário: 4 Droplets para a estrutura base (1 Master + 3 Nodes)
 
### GITLAB

  Para a execução do processo, são necessários alguns parâmetros:

| Param | Descricao | *Atenção* | Terraform=tf / Ansible = ans |
|-------|-----------|---|---|
| TF_VAR_num_k3s_masters | Especifica o numero de nos master para o cluster da aplicacao | Recomendado usar na hora do deploy, para determinar o tamanho da estrutura. O Terraform já tem uma regra para evitar colocar numeros altos de nos   | tf |
| TF_VAR_num_k3s_nodes   | Especifica o numero de nos que atuarao como worker no cluster | Recomendado usar na hora do deploy, para determinar o tamanho da estrutura. O Terraform já tem uma regra para evitar colocar numeros altos | tf |
| TF_VAR_num_k3s_nodes_db   | Especifica o numero de nos que atuarao como worker para o banco no cluster | Recomendado usar na hora do deploy, para determinar o tamanho da estrutura. O descritor do Terraform já tem uma regra para evitar colocar numeros altos | tf |
| TF_VAR_do_token | API Token da Digital Ocean | Fica protegido na parte de variaveis do CI/CD do Gitlab. Deixar com opção: **Protected** e **Masked** | tf |
| TF_VAR_ssh_k8s_keys | Chave **Pública** para acesso SSH em todos os nós publicados (master+workers), será registrado na DO e o Droplet recebe a mesma como chave de acesso | Também usar na parte CI/CD do GitLAB, evitando o uso comum e deixando protected. | tf |
| SSH_PRIVATE_KEY | Chave **Privada** para conexão do Ansible com o nó, para término da aplicação das configurações, instalação de recursos e etc. | Armazenada no CI/CD | ans |
| G_RUNNER_TOKEN | Token obtido no GITLAB CI, para registro do Runner em: CI/CD -> Specific Runners | Armazenada no CI/CD (no caso aqui eu coloquei no grupo e não apenas no projeto para que pudesse ser usado em outros subprojetos)| ans |
| ZEROTIER_NETWORK | Numero de identificacao da rede que permitirá a conexão com o sistema | Amazenada no CI/CD (dentro do grupo para uso geral) | ans |
| PG_PASSWORD | Senha do usuário da base || ans |
| PG_ADMIN_PASSWORD | Senha do usuário postgres - administrador | Nunca usar esse para o sistema, risco alto de problemas | ans |
| PG_USER | Nome do usuário da base | Nunca usar o usuário postgres | ans | 
| PG_REP_USER | Usuário da base de replicacao | Use um completamente diferente dos outros já criados | ans | 
| PG_REP_PASSWORD | Senha do usuário da base de replicacao || ans | 



### Segurança

 - Os pacotes envolvidos são todos analisados pela parte de Segurança no CI diposto pelo Gitlab (Security & Compliance)
 - É criada a regra de firewall para permitir o acesso somente do LoadBalancer aos pontos.
 - É criada uma bridge para permitir que o administrador da infra acesse através do [Zerotier](https://www.zerotier.com/), id da rede deve ser fornecido como parâmetro citado anteriormente
 - Foi derivada uma nova imagem docker para o Ansible para que a mesma seja verificada e analisada. Mudou também de CentOS para Alpine.

![sec]()./pics/seguranca.png)

### Zerotier

 - Provê conexão P2P
 - É necessário autorizar a conexão dos droplets à rede

![Auth](./pics/zerotier-auth.png)

### Deploy da infra

 - Após as verificações de testes base é necessário aplicar o deploy da infra, é um processo manual propositalmente

![graf](./pics/desafio_pulse.png)

### Storage As Software

 - Adicionado e configurado o LongHorn para uso de Storage distribuido, utilizando o disco existente nos Droplets de Banco de Dados
 - Na criacao da infra é diferenciado cada node ( para apps e para banco ), no node de banco é adicionado um volume estrar e etiquetado como tal. O sistema identifica e usa esse volume para replicacao dos dados


### Gitlab - Runner On Demand

  - Instalado para receber solicitacoes de build do projeto, bastando indicar a tag "ci-desafio" para uso do mesmo

![runner](./pics/gitlab-runner.png)

### Destroy da Infra

 - Para completa remoção basta na pipeline iniciar o step destroy.
